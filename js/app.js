var placas="0";
var noempleado="0";
var tipo="NADA";
var carrito;
var iConductor="";

$(document).ready(
	function(){
		$('.footer').hide();
	});


function buscar()
{
//asignar a variable
placas = $('#placas').val();

	if(placas != "")
	{
		$.ajax({
	        url: "http://74.208.100.107/EstacionamientoFK/webservice/vista_autos.php",
	        type: "POST",
	        crossDomain: true,
	        data: {Accion:'vista_autos_Listar_Placas',Placas:placas},
	        dataType: "json",
	        success:function(result){
	        	if(result.length>0)
	        	{
	        		carrito=result[0];
	        		iConductor = carrito.iConductor;
	        		$('#page1').find('.ui-btn-active').removeClass('ui-btn-active ui-focus');
	        		$('#lblplacas').text(carrito.Placas);
	        		$('#color').text(carrito.Color);
	        		$('#noempleado').text(carrito.NoEmpleado);
	        		$('#nombre').text(carrito.Nombre+" "+carrito.Apellidos);
	        		$('#departamento').text(carrito.Area);
	        		$.mobile.changePage("#page2");
	        		$('#img_Auto').attr('src','http://74.208.100.107/EstacionamientoFK/admin/php/_archivos/'+carrito.Foto_Auto);

	        		placas = "";
	        	}
	        	else
	        	{
	        		$('#pg1texto').text('Placas No Encontradas');
	        		$('#pg1footer').fadeIn('slow');
	        		setTimeout( "jQuery('.footer').fadeOut('slow');",3000 );
	        		$('#page1').find('.ui-btn-active').removeClass('ui-btn-active ui-focus');
	        	}
	            
	        },
	        error:function(xhr,status,error){
	            alert(status);
	            alert(xhr);
	            alert(error);
	             $('#page1').find('.ui-btn-active').removeClass('ui-btn-active ui-focus');
	        }
	    });
	}
	else
	{
		iConductor = "";
		//alert('Placas vacias');
		$('#pg1texto').text('Placas Vacias');
		$('#pg1footer').show();
		 setTimeout( "jQuery('.footer').hide();",3000 );
	}
}

//reportar placas
function reportar()
{
    if(iConductor != "")
   {
	   $.ajax({
	        url: "http://74.208.100.107/EstacionamientoFK/webservice/Mail/EnviarMail.php",
	        type: "POST",
	        crossDomain: true,
	        data: {EnviarMail:'EnviarMail',Reporte:'',Longitud:'',Latitud:'',Foto:'',iUsuario:'1',iConductor:iConductor},
	        success:function(result)
	        { 
	            //alert(result);
	            iConductor = "";
	            $('#pg2texto').text('Registrado');
	            $('#pg2footer').fadeIn('slow');
	            setTimeout( "jQuery('.footer').fadeOut('slow');",3000 );
	            limpiar();
	        },
	        error:function(xhr,status,error){
	            //alert(status);
	            $('#pg2texto').text('Error');
	            $('#pg2footer').fadeIn('slow');
	            setTimeout( "jQuery('.footer').fadeOut('slow');",3000 );
	             $('#page3').find('.ui-btn-active').removeClass('ui-btn-active ui-focus');
	        }
	    });
   }
   else
   {
   		alert("Placas vacias");
   }
}
//registar acceso
function registrar()
{    
    placas = $('#regplacas').val();
    noempleado = $('#regnoempleado').val();
    tipo=$( "#toggleswitch1" ).val();
   
   if(placas != "")
   {
	   $.ajax({
	        url: "http://74.208.100.107/EstacionamientoFK/webservice/logs.php",
	        type: "POST",
	        crossDomain: true,
	        //data: {Accion:'logs_Registrar',iEstacionamiento:1,iUsuario:1,Placas:placas,NoEmpleado:noempleado,Tipo_Log:tipo},
	        data: {Accion:'logs_Registrar',iEstacionamiento:1,iUsuario:1,Placas:placas,NoEmpleado:noempleado,Tipo_Log:tipo},
	        success:function(result)
	        { 
	        	if(result=='Placas no registradas')
	        	{
	        		$('#pg3texto').text('Placas no registradas');
	        		$('#pg3footer').fadeIn('slow');
	        		setTimeout( "jQuery('.footer').fadeOut('slow');",3000 );
	        		garbageCollector();
	        	}
	        	else
	        	{
	        		$('#pg3texto').text(result);
	        		$('#pg3footer').fadeIn('slow');
	        		setTimeout( "jQuery('.footer').fadeOut('slow');",3000 );
	        		garbageCollector();
	        	}
	            $('#page3').find('.ui-btn-active').removeClass('ui-btn-active ui-focus');
	            placas = "";
	        },
	        error:function(xhr,status,error){
	            //alert(status);
	             $('#page3').find('.ui-btn-active').removeClass('ui-btn-active ui-focus');
	             garbageCollector();
	        }
	    });
   }
   else
   {
   		alert("Placas vacias");
   }
}

function esconder()
{
	$('.footer').hide();
}
//limpiar pantallas
function garbageCollector()
{
	carrito="";
	$('input[type=text]').each(function() {
        $(this).val('');
    });
    $('span[type=text]').each(function() {
        $(this).val('');
    });

	/*$('#placas').text("");
	$('#lblplacas').text("");
	$('#color').text("");
	$('#noempleado').text("");
	$('#nombre').text("");
	$('#departamento').text("");*/
}

function limpiar()
{
	garbageCollector();
	$.mobile.changePage("#page1");
}